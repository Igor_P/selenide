package pageObjectPages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$$x;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

public class Main {

    ElementsCollection menuSections() {
        return $$x("//*[contains(@class, 'menu-categories_type_main')]//a");
    }

    @Step("The method clicks on 'Smartphones, TV and Electronics' section")
    public SmartphonesTVAndElectronics clickSmartphonesTvAndElectronics() {
        menuSections().get(1).click();
        return new SmartphonesTVAndElectronics();
    }
}
