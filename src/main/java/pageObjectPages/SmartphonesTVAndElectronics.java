package pageObjectPages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$$x;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

public class SmartphonesTVAndElectronics {

    ElementsCollection tilesHeading() {
        return $$x("//*[contains(@class, 'tile-cats__heading')]");
    }

    @Step("The method clicks on 'Smartphones' section")
    public Smartphones clickSmartphones() {
        tilesHeading().get(0).click();
        return new Smartphones();
    }
}
