package pageObjectPages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

public class Smartphones {

    SelenideElement sortingField() {
        return $x("//rz-sort");
    }

    SelenideElement sortingValues(int valueId) {
        return sortingField().find(By.xpath(".//*[contains(@value, '" + valueId + "')]"));
    }

    ElementsCollection titleOfProducts() {
        return $$x("//span[@class='goods-tile__price-value']");
    }

    SelenideElement preloader() {
        return $x("//*[@class='preloader']");
    }

    ElementsCollection productTitlesByLabel(String promoLabel) {
        return $$x("//*[@class='goods-tile__inner' and ./span[contains(text(),'" + promoLabel + "')]]//span[@class='goods-tile__title']");
    }

    ElementsCollection productPricesByLabel(String promoLabel) {
        return $$x("//*[@class='goods-tile__inner' and ./span[contains(text(),'" + promoLabel + "')]]//span[@class='goods-tile__price-value']");
    }

    @Step("The method clicks on page number {pageNumber}")
    public Smartphones clickPageNumber(int pageNumber) {
        $x("//*[contains(@class, 'pagination__item') and a[contains(text(),'" + pageNumber + "')]]").click();
        preloader().shouldBe(not(visible));
        return new Smartphones();
    }

    @Step("The method saves products data to map from first page till page {lastPageNumber} ")
    public Smartphones saveProductsDataToMapFromPageOneTillPageValue(Map<String, String> map, String promoLabel, int lastPageNumber) {
        for (int startPageNumber = 1; startPageNumber <= lastPageNumber; startPageNumber++) {
            int index = 0;
            while (index < productTitlesByLabel(promoLabel).size()) {
                String title = productTitlesByLabel(promoLabel).get(index).getText();
                String price = productPricesByLabel(promoLabel).get(index).getText();
                index++;
                map.put(format("%s ", title), format(" %s", price) + "\n");
            }
            if (startPageNumber < lastPageNumber) {
                clickPageNumber(startPageNumber + 1);
            }
        }
        return this;
    }

    @Step("The method saves price of smartphones to list")
    public Smartphones savePriceOfSmartphonesToList(List<String> list) {
        for (SelenideElement productTitle : titleOfProducts()) {
            String price = productTitle.getText();
            list.add(price);
        }
        return this;
    }

    @Step("The method sorts values in given list from largest to smallest")
    public Smartphones sortAndSavePricesToListFromHighToLow(List<String> originalList, List<String> listToSort) {
        listToSort.addAll(originalList);
        listToSort.sort(Collections.reverseOrder());
        return this;
    }

    @Step("The method clicks on sorting type by ID: {valueId}")
    public Smartphones clickSortingByValueId(int valueId) {

        /**
         * value ids:
         * 1 = cheap
         * 2 = expensive
         * 3 = popularity
         * 4 = novelty
         * 5 = action (promotional)
         * 6 = rank
         */

        sortingValues(valueId).click();
        preloader().shouldBe(not(visible));
        return new Smartphones();
    }
}
