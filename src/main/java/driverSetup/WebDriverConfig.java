package driverSetup;

import com.codeborne.selenide.Configuration;
import enums.Browsers;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

public class WebDriverConfig {

    static String browserFromJenkins = System.getenv("BROWSER");

    public static void initDriver(Browsers browser) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        Configuration.browserCapabilities = capabilities;
        Configuration.browserPosition = "0x0";
        Configuration.browserSize = "1440x900"; //for macOS 15 inches
        Configuration.timeout = 10000;
        Configuration.pageLoadTimeout = 20000;
        Configuration.browser = "chrome";
        Configuration.headless = false;
        boolean runWithSelenoid = false;

        if (runWithSelenoid) {
            capabilities.setCapability("enableVNC", true);
            Configuration.remote = "http://localhost:4444/wd/hub";
        }

        if (browserFromJenkins != null) {
            Configuration.browser = browserFromJenkins;
        } else {
            switch (browser) {
                case EDGE: {
                    Configuration.browser = "edge";
                    return;
                }
                case CHROME: {
                    Configuration.browser = "chrome";
                    return;
                }
                case FIREFOX: {
                    Configuration.browser = "firefox";
                    return;
                }
                case SAFARI: {
                    Configuration.browser = "safari";
                }
            }
        }
    }

    public static void initDriver() {
        String browserName = System.getProperty("browserName", "chrome");
        try {
            initDriver(Browsers.valueOf(browserName.toUpperCase()));
        } catch (IllegalArgumentException e) {
            System.err.println("This browser is not supported!!!");
            System.exit(-1);
        }
    }
}
