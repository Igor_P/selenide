package helpers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

public class FileWrite {

    private static final File products = new File("src/test/Products.txt");

    public static void writeToFile(Map<String, String> map) {
        try {
            FileWriter fileWriter = new FileWriter(products);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                fileWriter.write(String.valueOf(entry).replace("=", "-"));
            }
            System.out.println("File written: " + products.getName());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static File getProductsFile() {
        return products;
    }
}
