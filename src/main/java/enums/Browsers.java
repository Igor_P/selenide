package enums;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

public enum Browsers {

    CHROME,
    FIREFOX,
    EDGE,
    SAFARI
}
