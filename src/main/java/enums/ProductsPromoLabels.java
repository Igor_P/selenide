package enums;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

public enum ProductsPromoLabels {

    POPULAR("ТОП ПРОДАЖ"),
    NEW("НОВИНКА"),
    PROMOTION("АКЦИЯ");

    private final String promoLabel;

    ProductsPromoLabels(String promoLabel) {
        this.promoLabel = promoLabel;
    }

    public String getPromoLabel() {
        return promoLabel;
    }
}
