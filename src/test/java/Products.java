import helpers.FileWrite;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageObjectPages.Main;
import pageObjectPages.Smartphones;
import testsSetup.BaseClass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Selenide.open;
import static enums.ProductsPromoLabels.PROMOTION;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

public class Products extends BaseClass {
    List<String> listSmartphonePrices = new ArrayList<>();
    List<String> listSortingPricesDescending = new ArrayList<>();
    Map<String, String> mapOfTitlesAndPrices = new LinkedHashMap<>();
    Main main = new Main();
    Smartphones smartphones = new Smartphones();


    @BeforeClass
    public void beforeClass() {
        open("https://rozetka.com.ua");
        FileWrite.getProductsFile().delete();
    }

    @Owner("Igor Petelsky")
    @Test
    @Description("Write to file data of sorted smartphones by type 'From high to low' from 1-3 pages")
    public void writeToFileSortedSmartphones() {
        main.clickSmartphonesTvAndElectronics()
                .clickSmartphones()
                .clickSortingByValueId(2)
                .savePriceOfSmartphonesToList(listSmartphonePrices)
                .sortAndSavePricesToListFromHighToLow(listSmartphonePrices, listSortingPricesDescending);
        Assert.assertEquals(listSmartphonePrices, listSortingPricesDescending);
        smartphones.saveProductsDataToMapFromPageOneTillPageValue(mapOfTitlesAndPrices, PROMOTION.getPromoLabel(), 3);
        Assert.assertFalse(mapOfTitlesAndPrices.isEmpty());
//        FileWrite.writeToFile(mapOfTitlesAndPrices);
//        Assert.assertTrue(FileWrite.getProductsFile().exists());
    }
}
