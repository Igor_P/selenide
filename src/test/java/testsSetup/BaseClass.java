package testsSetup;

import driverSetup.WebDriverConfig;
import helpers.Listener;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import static com.codeborne.selenide.Selenide.closeWebDriver;

/**
 * Created by Igor Petelsky on Jun, 2022
 */

@Listeners(Listener.class)
public class BaseClass {

    @BeforeSuite
    public void setUpBrowser() {
        WebDriverConfig.initDriver();
    }

    @AfterSuite
    public void afterSuite() {
        closeWebDriver();
    }
}
