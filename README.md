# Run tests from console via maven:

Command: mvn test -Dtest={testClassName}

Example: mvn test -Dtest=Products

To select browser, add parameter -DbrowserName={browser}

Example: mvn test -Dtest=Products -DbrowserName=firefox

# Run test suite from console via maven:

Example: mvn test -DTEST_SUITE=Products -DbrowserName=firefox

